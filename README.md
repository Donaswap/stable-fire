![Stars](https://img.shields.io/gitlab/stars/Donaswap/stable-fire?style=social)
![Forks](https://img.shields.io/gitlab/forks/Donaswap/stable-fire?style=social)
![Contributors](https://img.shields.io/gitlab/contributors/donaswap/stable-fire)
![Issues](https://img.shields.io/gitlab/issues/open/Donaswap/stable-fire)
![Merge requests](https://img.shields.io/gitlab/merge-requests/open/Donaswap/stable-fire)
![Last Commit](https://img.shields.io/gitlab/last-commit/Donaswap/stable-fire)
![Discord](https://img.shields.io/discord/851473572772970527?label=Discord)
![Twitter Follow](https://img.shields.io/twitter/follow/thefirechain?style=social)

# Stable Fire (FUSD)

The (FUSD) Stable Coin Source Codes for the Firechain Network [Stable Fire (FUSD)](https://fusd.thefirechain.com/)

## Single file smart contract
All Stable Fire (FUSD) smart contracts in a single file


## Smart contracts

The folder contains all seperated solidity smart contracts from the single file solidity contract

---
## License

[![License: GPL v3.0](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

This project is licensed under the GNU General Public License v3.0. See the [LICENSE](LICENSE) file for details.